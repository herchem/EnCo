# -*- coding: utf-8 -*-
import sys
sys.path.append('/home/hernan/Bibliotecas/Python')
from elements.elements import ELEMENTS as elemento
import os
import numpy as np


# --------------------------------------------------------------------
# From gan library
# --------------------------------------------------------------------
# Calcula el centro de masa de la molécula
def cm(molec):
    M, x, y, z = 0., 0., 0., 0.
    for atomo in molec:
        mi = elemento[atomo[0]].mass
        M = M + mi
        x = x + mi * float(atomo[1])
        y = y + mi * float(atomo[2])
        z = z + mi * float(atomo[3])
    return([x / M, y / M, z / M])

def trcm(molecula):
    tr = cm(molecula)
    mol = []
    for atomo in molecula:
        x = float(atomo[1]) - tr[0]
        y = float(atomo[2]) - tr[1]
        z = float(atomo[3]) - tr[2]
        mol.append([atomo[0], x, y, z])
    return(mol)

def only_xyz(molec):
    natom = len(molec)
    xyz = []
    for i in range(natom):
        xyz.append(
            [float(molec[i][1]), float(molec[i][2]), float(molec[i][3])])
    return(xyz)

def conn_atom(at1, at2, **kwargs):
    if ('tol' in kwargs):
        tol = float(kwargs['tol'])/100.
    else:
        tol = 0.2
    dc = elemento[at1[0]].covrad + elemento[at2[0]].covrad
    dr = d( only_xyz([at1]) , only_xyz([at2]) )
    if (dr <= dc *(1.+tol)):
        return(True)
    else:
        return(False)

def bonds_mol(mol, **kwargs):
    if ('tol' in kwargs):
        tol = kwargs['tol']
    else:
        tol = 20.

    bonds = set()
    for i in range(0,len(mol)-1):
        for j in range(i+1,len(mol)):
            if conn_atom(mol[i],mol[j],tol=tol):
                bonds.add((i,j))
    return(bonds)

# --------------------------------------------------------------------
# From mopac and utils library
# --------------------------------------------------------------------
# molecule specified as list TO xyz file
def listtoxyz(lista,archivo,**kwargs):
   salida = open(archivo,"w")
   print(len(lista),file=salida)
   if 'comment' in kwargs:
      print(kwargs['comment'],file=salida)
   else:
      print("",file=salida)
   for at in lista:
      print(at[0],at[1],at[2],at[3],file=salida)
   return
#Devuelve la geometria xyz de un .arc
def arc_coords(file):
    lst = []
    aux = False
    for line in reversed(file):
        if len(line.strip()) > 0:
            lline = line.split()
            lst.append([lline[0], lline[1], lline[3], lline[5]])
            aux = True
        else:
            if (aux):
                lst.reverse()
                return(lst)
#Devuelve el calor de formación en kcal/mol de un .arc
def arc_hof(file):
    lst = []
    aux = False
    for line in file:
        if "HEAT OF FORMATION" in line:
            return(float(line.split()[4]))




def pairing(P,V):
    Q = np.copy(V)
    pair = np.zeros(P.shape[0],dtype=np.int)
    for i in range(0,P.shape[0]):
        p_i = np.tile(P[i,:],(P.shape[0],1))
        normas = np.linalg.norm(p_i-Q, axis=1)# axis=0 ?
        me_quedo_con = np.argmin(normas)
        pair[i] = me_quedo_con
        Q[me_quedo_con,:] = float("Inf")
        #np.delete(Q, (me_quedo_con), axis=0)

    return(P[pair])

np.set_printoptions(suppress=True)
np.set_printoptions(formatter={'float': '{: 0.1f}'.format})



#--------------------------------------------------------------
# Find the best rotation matrix according to Kabsch algorithm
# https://en.wikipedia.org/wiki/Kabsch_algorithm
def kabsch(P, Q):
    # Computation of the covariance matrix
    A = (P.T).dot(Q)
    V, S, W = np.linalg.svd(A)
    # Recordar que det(A B) = det(A) det(B), y que det(A^T) = det(A)
    I = np.identity(3)
    I[2,2] = np.sign( np.linalg.det(V) * np.linalg.det(W) )
    U = ((W.T).dot(I)).dot(V.T)
    return(U)


# Root Mean Squared deviation between arrays
def rmsd(A, B):
    return( np.sqrt( (1./A.size) * np.sum((A-B)**2)  ) )

# Centra las coordenadas para moleculas de un unico tipo de atomo
def center(A):
    return(A[:,:] - np.mean(A,axis=0) )

# Calcula el RMSD luego de rotar la molécula por el algoritmo
# de Kabsch
def kabsch_rmsd(P, Q):
    # Encuentra la matriz de rotación para P
    U = kabsch(P, Q)
    # Rota a P
    
    P = (U.dot(P.T)).T
    #P = P.dot(U)
    for i in range(0,3):
        P = pairing(P,Q)
        U = kabsch(P, Q)
        P = (U.dot(P.T)).T
    #print(P)
    #print(Q)
    # Calcula el RMSD entre P rotada y Q
    return(rmsd(P, Q))


#--------------------------------------------------------------
def d(p1, p2):
    p = np.array(p1) - np.array(p2)
    return(np.asscalar(np.sqrt((p).dot(p.T))))
def d_list(molec):
    n = molec.shape[0]
    lista = [ d(molec[i],molec[j]) for i in range(0, n-1) for j in range(i+1, n)]
    return(np.array(lista))





dir_main = os.getcwd()

directorios = sorted([name for name in os.listdir(dir_main) if os.path.isdir(os.path.join(dir_main, name))])


os.makedirs(dir_main+"/REPORT")


archivos_analizados = 0
archivos_a_borrar = 0

energias_minimas = []

for i in directorios:
    subdir = dir_main+"/"+i
    print("Trabajando sobre el directorio", i)
    os.chdir(dir_main+"/"+i)
    arc_files = sorted([name for name in os.listdir(subdir) if name.endswith('arc') ])
    


    mol_list = []
    mol_enteras = []

    for j in arc_files:
        inp   = open(j, "r")
        f = inp.readlines()
        inp.close()
        mol_entera = arc_coords(f)
        mol_entera = trcm(mol_entera)
        mol = only_xyz(mol_entera)
        mol_enteras.append(mol_entera[:])
        mol_list.append(np.array(mol))

    archivos_analizados += len(mol_list)



    eliminar=[]

    # Filter by connectivity
    # ----------------------

    connect_right = bonds_mol(mol_enteras[0])
   
    for k in range(1, len(mol_list)):
         connect_k = bonds_mol(mol_enteras[k])
         if ( connect_k !=  connect_right):
             eliminar.append(k)

    # Filter by geometry    
    for k in range(0, len(mol_list)-1):
        for j in range(k+1,len(mol_list)):
            if ( k not in eliminar and j not in eliminar):
                iguales = False
                rmse = kabsch_rmsd(mol_list[k],mol_list[j])
                print(arc_files[k],arc_files[j],rmse)
                if (rmse < 0.1):
                    iguales = True
                if (iguales): 
                    eliminar.append(j)
                    print(k,j)

    # Retain the following files:
    mantener = list( set(range(0,len(mol_list))) - set(eliminar) )


    # Guardo el valor de energía y el índice de mantener
    energy_array = np.zeros((len(mantener),2))
    for k, j in enumerate(mantener):
        energy_array[k,1] = arc_hof(open(arc_files[j]).readlines())
        energy_array[k,0] = float(j)
    
    minimo_tx = np.min(energy_array[:,1])
    
    energy_array[:,1] = energy_array[:,1] - np.min(energy_array[:,1])
    
    energias_minimas.append( (round(minimo_tx,2), i) )
    
    energy_array = energy_array[energy_array[:,1].argsort()]
    energy_file = open(dir_main+"/REPORT/"+i+"-summary.dat","w")
    print("Standard enthalpies of formation in kcal/mol, PM7", file=energy_file)
    print("Reference value: ", "%.2f" % round(minimo_tx,2), " kcal/mol", file=energy_file)
    
    

    print("", file=energy_file)
    for k, j in enumerate(energy_array[:,0]):
        print("%.2f" % round(energy_array[k,1],2),i+"-"+str(int(j))+".xyz",file=energy_file)
        listtoxyz(mol_enteras[int(j)],dir_main+"/REPORT/"+i+"-"+str(int(j))+".xyz",comment="Energía (PM7) respecto del rotámero más estable: "+str(energy_array[k,1])+ "kcal/mol. HOF del más estable: "+str("%.2f" % round(minimo_tx,2))+" kcal/mol.")
    

file_minimos = open(dir_main+"/REPORT/minimum.dat","w")
for k in range(0, len(energias_minimas)):
    print("%.2f" % round(energias_minimas[k][0],2), energias_minimas[k][1], file=file_minimos)
