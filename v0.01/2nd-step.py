# -*- coding: utf-8 -*-
import os
import pybel
var="PM7 CHARGE=0 SINGLET MMOK GNORM=0.01"

# Loops over directories writting MOPAC inputs
# from xyz files.
dir_main = os.getcwd()
for i in filter(os.path.isdir, os.listdir(dir_main) ):
    os.chdir(dir_main+"/"+i)
    subdir = dir_main+"/"+i
    print(subdir)
    xyz_files = os.listdir( subdir )
    for j in xyz_files:
        base = j[:-4]
        for mol in pybel.readfile( "xyz", j):
            mol.write("mopcrt", base+".dat",overwrite=True)
        os.system('sed -i "1s/.*/'+var+'/g" '+base+".dat")
        print(j)
        os.remove(j)
