# -*- coding: utf-8 -*-
import openbabel as ob
import pybel
from math import radians
import itertools
import os

import random

random.seed(666)

# nrot = Diehedral angles per rotatable bond
def gen_rot(name,nrot, **kwargs):
 for mol in pybel.readfile( "xyz", name):
   rl  = []
   for bd in ob.OBMolBondIter(mol.OBMol):
      if (bd.IsRotor()):
         rl.append([bd.GetBeginAtomIdx(),bd.GetEndAtomIdx()])
   if (len(rl) == 0):
       print("No rotatable bonds")
       return

   lr14 = [] 
   lt   = [] 
   for ln in rl:
      at2ID = ln[0]
      at3ID = ln[1]
      at2 = mol.atoms[at2ID-1]
      at3 = mol.atoms[at3ID-1]
      for at2Vec in ob.OBAtomAtomIter(at2.OBAtom):
         if (at2Vec.GetIdx() != at3ID):
            at1ID = at2Vec.GetIdx()
      for at3Vec in ob.OBAtomAtomIter(at3.OBAtom):
         if (at3Vec.GetIdx() != at2ID):
            at4ID = at3Vec.GetIdx()         
      lr14.append([at1ID,at4ID])
      lt.append(mol.OBMol.GetTorsion(at1ID,at2ID,at3ID,at4ID))
  
   angs = []
   for i in range(0,len(lt)):
      angs.append([])
      for n in range(0,nrot):
         angs[i].append(radians(lt[i]+360./float(nrot)*float(n)))

   if 'limit' in kwargs:
      limit = kwargs['limit']
   else:
      limit = None

   tabla = [] #Tabla de ángulos de rotación de cada combinación de ángulos dihedro      
   if limit is not None:
       j = -1
       for i in itertools.product(*angs):
           j = j + 1
           if j < limit:
               tabla.append(list(i))
           else:
               k = random.randint(0, j)
               if k < limit:
                  tabla[k] = list(i)
   else:
       for i in itertools.product(*angs):
           tabla.append(list(i))



   for k in range(0,len(tabla)):
      for l in range(0,len(tabla[k])):
         at1 = mol.atoms[lr14[l][0]-1]
         at2 = mol.atoms[rl[l][0]-1]
         at3 = mol.atoms[rl[l][1]-1]
         at4 = mol.atoms[lr14[l][1]-1]

         mol.OBMol.SetTorsion(at1.OBAtom,at2.OBAtom,at3.OBAtom,at4.OBAtom,tabla[k][l])
      print("Writting file for",str(k).zfill(4))
      mol.write("xyz", str(k).zfill(4)+".xyz",overwrite=True)
   return

dir_main = os.getcwd()
for i in os.listdir(dir_main):
    if i.endswith(".xyz"):
       inp_file = dir_main+"/"+i
       base = i[:-4]
       print("")
       print("FILE")
       print(base)
       os.system("mkdir "+ base)
       os.chdir(dir_main+"/"+base)
       gen_rot(inp_file,3)
       os.chdir(dir_main)


