## Introduction 

This program can be used for searching stable conformers of low energy. Its general approach to the problem is the following:

1. For each molecule having N rotatable bonds, it creates min[3^N, 3^6] rotamers from the respective previously supplied equilibrium geometries.
2. Optimizes the geometry of each new rotamer.
3. Returns a list of conformers sorted by energy showing the energy difference between each conformer and that of lowest energy.

### More details

1. The detection of rotatable bonds is done using OBabel. The creation of the conformers is performed by rotating dihedral angles (involving the rotatable bonds) in angles of 360º/3.
2. The geomtery is optimized using the PM7 semiempirical method employing MOPAC2016.
3. The number of posible rotamers increase exponentially (3^N). Large molecules may have a huge number of possible rotamers. In order to keep it in a manageable number, only 3^6 rotamers are created randomly using the Fisher & Yates[1] (modified by Durstenfeld[2]) algorithm.
4. This code can be modified to use other methods (instead of PM7) and different thresholds for the rotations and number of conformers. This should not be a major effort.

[1]Fisher, R. A., & Yates, F. (1963). tatistical tables for biological, agricultural and medical research., (6th ed).
[2] Durstenfeld, R. (1964). Algorithm 235: random permutation. Communications of the ACM, 7(7), 420.

## Installation 
In order to execuete EnCo, it must be installed in your system. The installation procedure only requires to copy some files and modify few paths.

You also need a working installation of Mopac 2012 or [MOPAC2016](http://openmopac.net/MOPAC2016.html). In the file `2nd-step.py` you should replace the line
`ejecutable="/opt/mopac/MOPAC2016.exe"`
in accordance with your MOPAC installation settings.

EnCo relies in some external libraries which are normally not included in standard Python installation. For EnCo to work properly you need to install them before. As EnCo uses both, Python 2 and Python3, you must be carefull about the versions to be installed.
Requirements:

-- [OpenBabel and Pybel](http://openbabel.org/wiki/Python)  (for Python2)
-- [Numpy](http://www.numpy.org/)  (for Python3)
-- elements.py (Supplied with the installation files)

Copy the file `elements.py` to an (preferable) empty folder. e.g.`/home/myself/elements_lib`. Change the line ` sys.path.append('/home/hernan/Bibliotecas/Python')` from the file
 `4th-step.py` specifying  the path of the file `elements.py` (e.g. ` sys.path.append('/home/myself/elements_lib`).


### Specific installation instructions for:
#### Ubuntu
##### 16.04


## Usage
- Once installed, copy the four `n*-step.py` files and the `.xyz` files (with the geometries of the molecules to be analyzed) into an empty directory. e.g. `/home/myself/new_folder` . 
- From the command line, change the working directory to previously created. e.g. `cd /home/myself/new_folder`
- Run sequentially the python scripts. They are Python2 scripts except for the last one that is coded in Python3. e.g. In Ubuntu 16.04 use something equivalent to

```
myself@mypc:~/new_folder$ python 1st-step.py
myself@mypc:~/new_folder$ python 2nd-step.py
myself@mypc:~/new_folder$ python 3rd-step.py
myself@mypc:~/new_folder$ python3 4th-step.py
```
*Note the "3" in the last line!*

That is all. A subdirectory named `REPORT` should have been created. You can check out the intermediate results at any step. To read a summary of the results look for the files (as many as molecules studied) containing the substring `summary`. An example of this file can be found below:

```
Standard enthalpies of formation in kcal/mol, PM7
Reference value:  61.85  kcal/mol

0.00 S-T13-sust-Cl-0-0.xyz
0.02 S-T13-sust-Cl-0-1.xyz
0.44 S-T13-sust-Cl-0-14.xyz
0.51 S-T13-sust-Cl-0-9.xyz
1.38 S-T13-sust-Cl-0-10.xyz
1.80 S-T13-sust-Cl-0-6.xyz
2.43 S-T13-sust-Cl-0-15.xyz
6.02 S-T13-sust-Cl-0-4.xyz
7.30 S-T13-sust-Cl-0-67.xyz
```


## How to cite? 

If you use this code, please cite it as:

Sánchez, H. R. (2017). Una exploración a las relaciones cuantitativas entre datos espectrométricos y actividad a través de descriptores mecano-cuánticos (Doctoral dissertation, Facultad de Ciencias Exactas).

## Licence
**EnCo**

Copyright (C) 2016 Hernán Sánchez

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.

hernan.sanchez.ds@gmail.com
